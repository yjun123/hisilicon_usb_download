## Hisilicon USB Download Tool

### 0x00 介绍

​	适用于海思(Hisilicon) SoC U-Boot USB下载的工具.

### 0x01 Udev配置及普通用户权限使用方法

1. 复制配置文件：将 [70-hisilicon-uboot-usb.rules](udev/70-hisilicon-uboot-usb.rules) 拷贝到 `/etc/udev/rules.d/`

2. 重新加载并应用规则：终端执行 `sudo udevadm control --reload-rules && sudo udevadm trigger` 重新加载规则并对系统中的设备应用新规则

### 0x02 构建和使用

1. 构建

```shell
cmake -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
cmake --build build
```

2. 使用

```shell
./build/hisilicon_usb_download pub/hi3519dv500_image_musl/nor_burn_table.xml -p kernel:rootfs
```

### 0x03  USB 协议

| 命令头 | 描述                  |
| ------ | --------------------- |
| USTART | 0xFA                  |
| UHEAD  | 0xFE                  |
| UDATA  | 0xDA                  |
| UTAIL  | 0xED                  |
| UCMD   | 0xAB, 执行U-Boot 命令 |
| UREQ   | 0xFB                  |
| UACK   | 0xAA                  |
| UNAK   | 0x55                  |
|        |                       |



| Variable              | DESCRIPTION                |
| --------------------- | -------------------------- |
| PARTATION_SIZE        | 分区文件大小（按照64K对齐) |
| PARTATION_ADDR_OFFSET | 分区地址偏移（读取分区XML) |
| PARTATION_ERASE_SIZE  | 分区大小 （读取分区XML)    |
|                       |                            |

#### 0x04 boot分区更新

TODO

#### 0x05 Sp Nor/Nand分区更新
0x00. 获取当前的硬件信息(打印spi nor/nand flash的信息)

   `getinfo spi/nand`

0x01. 将内存地址`0x41000000`开始长度为${PARTATION_SIZE}的内存清空为`0xFF`

   `mw.b 0x41000000 0xFF ${PARTATION_SIZE}`

0x02. 通过USB 下传分区内容

0x03. 执行Flash 探查

   `sf probe 0`

0x04. 执行Flash 擦除

   `sf erase ${PARTATION_ADDR_OFFSET} ${PARTATION_ERASE_SIZE}`

0x05. 将内存中的分区内容写入到 Flash

   `sf write 0x41000000 ${PARTATION_ADDR_OFFSET} ${PARTATION_SIZE}`

0x06. 更新结束，复位

   `reset`

#### 0x06 eMMC 分区更新

0x00. 将内存地址`0x41000000`开始长度为 ${PARTATION_SIZE} 的内存清空为`0xFF`

   `mw.b 0x41000000 0xFF ${PARTATION_SIZE}`

0x01. 通过USB 下传分区内容

0x02. 将内存中的分区内容写入到 eMMC0,每次往块地址（每个块大小512字节），写入65536块数据

   `mmc write 0x0 0x41000000 0xa0000 0x10000`

   `mmc write 0x0 0x41000000 0xb0000 0x10000`

0x03. 更新结束，复位

   `reset`

#### 0x07 注意事项

U-Boot枚举的usb下载设备（下面简称U-Boot模式）和BootROM枚举的usb下载设备（下面简称BootROM模式）在描述符上面有差异,主要表现在:

   U-Boot模式的 manufacturer_string 为 VENDOR, BootROM模式的则为 Vendor(?)
   
   U-Boot模式**不支持**更新boot分区, BootROM则仅支持更新boot分区(?)
   
   U-Boot模式下的通信协议和BootROM模式下的有差异 （TODO，描述U-Boot模式和BootROM模式下的通信协议差异)
   
   
#### 0x08 TODO

1. 支持更新boot分区 (boot_image.bin 包含 gsl,regbin, U-Boot, teeos(?), atf(?))
