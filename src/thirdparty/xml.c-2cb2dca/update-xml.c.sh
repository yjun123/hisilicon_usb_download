#!/bin/env bash
PRJ_NAME=xml.c
rm -rf ./build

cmake -DCMAKE_BUILD_TYPE=Release -B build
cmake --build build

mkdir -p ../../../lib/xml.c/{lib,include}
cp -v build/libxml.a ../../../lib/xml.c/lib
cp -v src/xml.h ../../../lib/xml.c/include
