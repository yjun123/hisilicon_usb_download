#ifndef _SY_COMMON_H_
#define _SY_COMMON_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <syslog.h>
#include <stdarg.h>

#define SUCCESS 0
#define FAILURE (-1)

#define TRUE (1)
#define FALSE (0)

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

// #define SL_DBG(string, args...) do { openlog(MODULE_NAME, LOG_PID|LOG_CONS, LOG_LOCAL0); syslog(LOG_DEBUG, "%s(%d) [%s]: "#string, __FILE__, __LINE__, __FUNCTION__, ##args); closelog(); } while(0)
// #define SL_ERR(string, args...) do { openlog(MODULE_NAME, LOG_PID|LOG_CONS, LOG_LOCAL0); syslog(LOG_ERR, "%s(%d) [%s]: "#string, __FILE__, __LINE__, __FUNCTION__, ##args); closelog(); } while(0)
#define SL_DBG(string, args...) printf("\033[34m" string "\033[0m", ##args)
#define SL_ERR(string, args...) printf("\033[0;31m [%s %d]exec func:" string "\033[0m", __func__, __LINE__, ##args)

#define P_CLR_NONE "\033[0m"
#define P_CLR_RED "\033[0;31m"
#define P_CLR_BLUE "\033[0;34m"

#define INFO(string, args...) printf("\033[33m" string "\033[0m", ##args)
#define SY_DBG(string, args...) printf("\033[34m" string "\033[0m", ##args)
#define SY_ERR(string, args...) printf("\033[0;31m [%s %d]exec func:" string "\033[0m", __func__, __LINE__, ##args)

#define SY_CHECK(b, action)      \
    {                            \
        if (unlikely(!(b)))      \
        {                        \
            SL_ERR("%s \n", #b); \
            action;              \
        }                        \
    }

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) ((sizeof(a) / sizeof(a[0])))
#endif

#ifndef CEILING_2_POWER
#define CEILING_2_POWER(x, a) (((x) + ((a) - 1)) & (~((a) - 1)))
#endif

#ifndef FLOOR_2_POWER
#define FLOOR_2_POWER(x, a) ((x) & (~((a) - 1)))
#endif

#ifndef ARRARY_SIZE
#define ARRARY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#endif

#ifndef ALIGN_UP
#define ALIGN_UP(x, align) (((x) + ((align) - 1)) & ~((align) - 1))
#endif

#ifndef ALIGN_BACK
#define ALIGN_BACK(x, a) ((x) / (a) * (a))
#endif

#ifndef ALIGN_DOWN
#define ALIGN_DOWN(x, align_to) ((x) & ~((align_to) - 1))
#endif

#ifndef BIT
#define BIT(x) (1 << (x))
#endif

#define CLOSE(x)      \
    do                \
    {                 \
        if (x > 0)    \
        {             \
            close(x); \
            (x) = -1; \
        }             \
    } while (0)
#define FCLOSE(x)       \
    do                  \
    {                   \
        if (x)          \
        {               \
            fclose(x);  \
            (x) = NULL; \
        }               \
    } while (0)

#define FREE(x)       \
    do                \
    {                 \
        if (x)        \
        {             \
            free(x);  \
            x = NULL; \
        }             \
    } while (0)

#ifdef __cplusplus
}
#endif
#endif
