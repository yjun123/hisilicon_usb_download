/**
 * @file xml_parse.c
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief XML解析子模块
 * @version 0.1
 * @date 2023-11-08
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>

#include "common.h"
#include "fileio.h"
#include "xml.h"
#include "xml_parse.h"

static const char *g_unknown_str = "unknown";
static const char g_flash_type_str[][8] = {
    "spi",
    "nand",
    "emmc",
};

static const char g_file_system_str[][8] = {
    "none",
    "jffs2",
    "ext4",
};

static int get_xml_size(char *xmlPath)
{
    int ret;
    struct stat statbuf;

    SY_CHECK(xmlPath, return FAILURE);

    ret = stat(xmlPath, &statbuf);

    if (ret)
    {
        printf("get size of %s failed\n", xmlPath);
        return FAILURE;
    }

    return statbuf.st_size;
}

const char *reverse_attr_to_string(attr_enum_e attr_enum, int enum_value)
{
    SY_CHECK(attr_enum < ATTR_BUTT, return g_unknown_str);

    switch (attr_enum)
    {
    case ATTR_FLASH_TYPE:
        SY_CHECK(enum_value < FLASH_TYPE_BUTT, return g_unknown_str);
        return g_flash_type_str[enum_value];
        break;
    case ATTR_FILE_SYSTEM:
        SY_CHECK(enum_value < FILE_SYSTEM_BUTT, return g_unknown_str);
        return g_file_system_str[enum_value];
        break;
    default:
        break;
    }

    return g_unknown_str;
}

static int get_value_from_attr(partition_t *pstPartition, uint8_t *name, uint8_t *value)
{
    SY_CHECK(pstPartition, return FAILURE);
    SY_CHECK(name, return FAILURE);
    SY_CHECK(value, return FAILURE);

    if (!strcmp("Sel", name))
    {
        // FIXME 有效转换检查
        pstPartition->sel = atoi(value);
    }
    else if (!strcmp("PartitionName", name))
    {
        // FIXME 拷贝长度检查
        strcpy(pstPartition->partitionName, value);
    }
    else if (!strcmp("FlashType", name))
    {
        switch (value[0])
        {
        case 's':
            pstPartition->flashType = FLASH_TYPE_SPI;
            break;
        case 'n':
            pstPartition->flashType = FLASH_TYPE_NAND;
            break;
        case 'e':
            pstPartition->flashType = FLASH_TYPE_EMMC;
            break;
        default:
            // TODO wrong flash type mention
            return 1;
        }
    }
    else if (!strcmp("FileSystem", name))
    {
        switch (value[0])
        {
        case 'n':
            pstPartition->fileSystem = FILE_SYSTEM_NONE;
            break;
        default:
            // TODO wrong file system mention
            return 1;
        }
    }
    else if (!strcmp("Start", name))
    {
        // FIXME 有效转换检查
        pstPartition->startAddr = strtol(value, NULL, 0);
    }
    else if (!strcmp("Length", name))
    {
        // FIXME 有效转换检查
        pstPartition->length = strtol(value, NULL, 0);
    }
    else if (!strcmp("SelectFile", name))
    {
        // FIXME 拷贝长度检查
        strcpy(pstPartition->selectFile, value);
    }
    else
    {
        // FIXME wrong attr type mention
        return 1;
    }

    return 0;
}

static int burn_xml_parse(uint8_t *pXmlBuff, uint32_t xmlLen, partition_info_t *pstPartitionInfo)
{
    struct xml_document *document;

    SY_CHECK(pXmlBuff, return FAILURE);
    SY_CHECK(pstPartitionInfo, return FAILURE);

    document = xml_parse_document(pXmlBuff, xmlLen);
    if (NULL == document)
    {
        printf("parse xml failed\n");
        return FAILURE;
    }

    struct xml_node *part_info_node = xml_document_root(document);
    // FIXME node name check

    size_t node_number = xml_node_children(part_info_node);
#if DEBUG
    printf("# partition count %ld\n", node_number);
#endif
    for (int i = 0; i < node_number; i++)
    {
        partition_t *pstPartition = &pstPartitionInfo->partitions[i];
        struct xml_node *part_node = xml_node_child(part_info_node, i);
        struct xml_string *part_name = xml_node_name(part_node);
        // FIXME node name check
#if DEBUG
        uint8_t *part_name_str = calloc(xml_string_length(part_name) + 1, sizeof(uint8_t));
        xml_string_copy(part_name, part_name_str, xml_string_length(part_name));
        printf("## partition %d \"%s\"\n", i, part_name_str);
        free(part_name_str);
#endif
        size_t attr_size = xml_node_attributes(part_node);
        for (int idx = 0; idx < attr_size; idx++)
        {
            struct xml_string *attr_name = xml_node_attribute_name(part_node, idx);
            struct xml_string *attr_value = xml_node_attribute_content(part_node, idx);

            uint8_t *attr_name_str = calloc(xml_string_length(attr_name) + 1, sizeof(uint8_t));
            uint8_t *attr_value_str = calloc(xml_string_length(attr_value) + 1, sizeof(uint8_t));

            xml_string_copy(attr_name, attr_name_str, xml_string_length(attr_name));
            xml_string_copy(attr_value, attr_value_str, xml_string_length(attr_value));

#if DEBUG
            printf("   %s ===> %s\n", attr_name_str, attr_value_str);
#endif
            if (get_value_from_attr(pstPartition, attr_name_str, attr_value_str))
            {
                printf("get_value_from_attr failed %s = %s\n", attr_name_str, attr_value_str);
                free(attr_name_str);
                free(attr_value_str);
                goto fail;
            }
            free(attr_name_str);
            free(attr_value_str);
        }
    }
    pstPartitionInfo->partitionCount = node_number;

    xml_document_free(document, true);
    return SUCCESS;
fail:
    xml_document_free(document, true);
    return FAILURE;
}

int burn_table_read(char *xmlPath, partition_info_t *pstPartitionInfo)
{
    int len;
    int read_len;
    FILE *pfXml = NULL;
    uint8_t *xmlBuff = NULL;

    SY_CHECK(xmlPath, return FAILURE);
    SY_CHECK(pstPartitionInfo, return FAILURE);

#ifdef DEBUG
    printf("reading partition info from xml %s...\n", xmlPath);
#endif

    len = get_xml_size(xmlPath);
    if (len <= 0)
    {
        printf("wrong size of xml %dB\n", len);
        goto fail;
    }

    xmlBuff = malloc(len);
    if (NULL == xmlBuff)
    {
        printf("malloc for xml buff failed\n");
        goto fail;
    }

    pfXml = fopen(xmlPath, "r");
    if (NULL == pfXml)
    {
        printf("open xml %s failed\n", xmlPath);
        goto fail;
    }

    get_dir_path(xmlPath, pstPartitionInfo->fwDirPath);

    memset(xmlBuff, 0, len);
    read_len = fread(xmlBuff, sizeof(char), len, pfXml);
    if (read_len != len)
    {
        printf("error: read len %d byte(s) is not equal to xml size %d byte(s).\n", read_len, len);
        goto fail;
    }

#ifdef DEBUG
    printf("read xml %s size %dB\n", xmlPath, len);
#endif

#if DEBUG
    printf("xml %s ====> %s\n", xmlPath, xmlBuff);
#endif

    if (burn_xml_parse(xmlBuff, len, pstPartitionInfo))
    {
        printf("parse burn xml failed\n");
        goto fail;
    }

    /* xmBuff has been freed in burn_xml_parse func */
    xmlBuff = NULL;
    return SUCCESS;

fail:
    xmlBuff = NULL;
    return FAILURE;
}

/**
 * @brief 打印读取解析后的分区烧录信息
 *
 * @param pstPartitionInfo
 */
void burn_table_dump(partition_info_t *pstPartitionInfo)
{
    int partitionCount = pstPartitionInfo->partitionCount;
    partition_t *pstPartition = NULL;

    SY_CHECK(pstPartitionInfo, return);

    printf("Sel PartitionName FlashType FileSystem Start      Length     SelectFile\n");
    for (int i = 0; i < partitionCount; i++)
    {
        pstPartition = &pstPartitionInfo->partitions[i];
        printf("%-3d %-13s %-9s %-10s 0x%08x 0x%08x %s\n", pstPartition->sel, pstPartition->partitionName,
               reverse_attr_to_string(ATTR_FLASH_TYPE, pstPartition->flashType),
               reverse_attr_to_string(ATTR_FILE_SYSTEM, pstPartition->fileSystem),
               pstPartition->startAddr, pstPartition->length,
               pstPartition->selectFile);
    }

    return;
}