#ifndef __TIMER_H__
#define __TIMER_H__

// 定时器开始计时
int timer_start(int index);

// 获取定时器运行时间(单位:微秒)
int timer_elapsed(int index);

#endif