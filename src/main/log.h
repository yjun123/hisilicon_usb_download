/**
 * @file log.h
 * @author Yan Jun (zngyanj@sunnyoptical.com)
 * @brief  日志控制
 * @version 0.1
 * @date 2023-12-11
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#ifndef __LOG_H__
#define __LOG_H__

typedef enum
{
    LOG_LVL_NONE = 0,
    LOG_LVL_ERR,
    LOG_LVL_WRN,
    LOG_LVL_INFO,
    LOG_LVL_DEBUG,
    LOG_LVL_BUTT,
} LOG_LEVEL_E;

#define ASCII_COLOR_RED "\033[1;31m"
#define ASCII_COLOR_WHITE "\033[1;37m"
#define ASCII_COLOR_YELLOW "\033[1;33m"
#define ASCII_COLOR_BLUE "\033[1;36m"
#define ASCII_COLOR_GREEN "\033[1;32m"
#define ASCII_COLOR_END "\033[0m"

extern int g_log_level;

#define LOG_DEBUG_PRINT(fmt, args...) ({\
    do {\
        if (g_log_level >= LOG_LVL_DEBUG)\
        {\
            printf(ASCII_COLOR_WHITE"[DEBUG]:%s[%d]: " fmt ASCII_COLOR_END"\n", __FUNCTION__, __LINE__,##args);\
        }\
    } while(0); })

#define LOG_INFO_PRINT(fmt, args...) ({\
    do {\
        if (g_log_level >= LOG_LVL_INFO)\
        {\
            printf(ASCII_COLOR_GREEN"[INFO]:%s[%d]: " fmt ASCII_COLOR_END"\n", __FUNCTION__, __LINE__,##args);\
        }\
    } while(0); })

#define LOG_WRN_PRINT(fmt, args...) ({\
    do {\
        if (g_log_level >= LOG_LVL_WRN)\
        {\
            printf(ASCII_COLOR_YELLOW"[WARN]:%s[%d]: " fmt ASCII_COLOR_END"\n", __FUNCTION__, __LINE__,##args);\
        }\
    } while(0); })

#define LOG_ERR_PRINT(fmt, args...) ({\
    do {\
        if (g_log_level >= LOG_LVL_ERR)\
        {\
            printf(ASCII_COLOR_RED"[ERR]:%s[%d]: " fmt ASCII_COLOR_END"\n", __FUNCTION__, __LINE__,##args);\
        }\
    } while(0); })

/**
 * @brief 设置日志等级
 *
 * @param level
 * @return int
 */
int log_set_level(int level);

#endif