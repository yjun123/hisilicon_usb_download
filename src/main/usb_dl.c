/**
 * @file usb_dl.c
 * @author Yan Jun (zngyanj@sunnyoptical.com)
 * @brief USB 烧录
 * @version 0.1
 * @date 2023-12-06
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#include "common.h"
#include "libusb.h"
#include "hisi_usb_dl_prot.h"
#include "usb_dl.h"
#include "xml_parse.h"
#include "fileio.h"
#include "log.h"
#include "timer.h"
#include "unistd.h"

#define MEMORY_TEMP_ADDR (0x41000000)
#define PARTITION_ALIGMENT_SIZE (0x10000) // 64KB
#define SEARCH_DEVICE_TIMEOUT 30          // 30s

#define UFU_TIMER 1
#define SEARCH_DEVICE_TIMER 2

typedef enum
{
    USB_DL_MODE_BOOT_ROM,
    USB_DL_MODE_U_BOOT,
    USB_DL_MODE_UNKNOWN,
} usb_dl_mode_e;

const char hisi_uboot_dl_manufacturer_string[] = "VENDOR";

const char custom_partitons_seperator = ':';

static const char g_non_upgraded_partitions[][32] = {
    "sec",
};

static int is_non_upgraded_partiton(char *partitionName)
{

    for (int i = 0; i < (sizeof(g_non_upgraded_partitions) / sizeof(g_non_upgraded_partitions[0])); i++)
    {
        if (!strcmp(partitionName, g_non_upgraded_partitions[i]))
        {
            return TRUE;
        }
    }

    return FALSE;
}

static int find_partiton_by_name(char *partitionName, partition_info_t *partitionInfo, partition_t **foundPartition)
{
    int partition_is_found = FALSE;
    int idx = 0;
    SY_CHECK(partitionName, return FAILURE);
    SY_CHECK(partitionInfo, return FAILURE);

    while (!partition_is_found)
    {
        if (idx >= partitionInfo->partitionCount)
            break;

        if (!strcmp(partitionName, partitionInfo->partitions[idx].partitionName))
        {
            partition_is_found = TRUE;
            *foundPartition = &partitionInfo->partitions[idx];
            break;
        }

        idx++;
    }

    return partition_is_found ? SUCCESS : FAILURE;
}

static int select_partition_by_name(char *partitionName, int select, partition_info_t *partitionInfo)
{
    partition_t *foundPartition = NULL;

    SY_CHECK(partitionName, return FAILURE);
    SY_CHECK(partitionInfo, return FAILURE);

    if (find_partiton_by_name(partitionName, partitionInfo, &foundPartition))
        return FAILURE;

    foundPartition->sel = select;

    return SUCCESS;
}

/**
 * @brief check current usb download mode
 *
 * @return int
 */
static int check_usb_download_mode(usb_dl_dev_t *usb_dl_dev)
{
    libusb_device **devs;
    libusb_device *dev;
    struct libusb_device_descriptor desc;
    int ret = USB_DL_MODE_UNKNOWN;
    uint8_t manufacturer[16];
    int i = 0;
    ssize_t cnt;

    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0)
    {
        goto exit;
    }

    while ((dev = devs[i++]) != NULL)
    {
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0)
        {
            fprintf(stderr, "failed to get device descriptor");
            goto exit;
        }

        if (hisi_usb_dl_is_valid_usb_id(desc.idVendor, desc.idProduct) == false)
        {
            LOG_DEBUG_PRINT("vendor id 0x%x, product id 0x%x is not matched", desc.idVendor, desc.idProduct);
            continue;
        }
        libusb_device_handle *device_handle = NULL;

        r = libusb_open(dev, &device_handle);
        if (r)
        {
            LOG_ERR_PRINT("libusb_open failed ret = %d", r);
            goto exit;
        }

        r = libusb_get_string_descriptor_ascii(device_handle, HISI_USB_DOWNLOAD_IMANUFACTURER, manufacturer, sizeof(manufacturer));
        if (r > 0)
        {
            if (!strcmp(hisi_uboot_dl_manufacturer_string, manufacturer))
            {
                ret = USB_DL_MODE_U_BOOT;
            }
            else
            {
                ret = USB_DL_MODE_BOOT_ROM;
            }
        }

        libusb_close(device_handle);
        if (ret != USB_DL_MODE_UNKNOWN)
            break;
    }

    usb_dl_dev->vendor_id = desc.idVendor;
    usb_dl_dev->product_id = desc.idProduct;

exit:
    if (devs)
        libusb_free_device_list(devs, 1);
    return ret;
}
/**
 * @brief USB 初始化
 *
 * @param device
 * @param verbose
 * @return int
 */
int usb_dl_init(libusb_device_handle **device, int verbose)
{
    SY_CHECK(!hisi_usb_dl_init(verbose), return FAILURE);

    uint8_t isDeviceFound = false;
    usb_dl_dev_t usb_dl_dev = {0};
    timer_start(SEARCH_DEVICE_TIMER);

    LOG_INFO_PRINT("Image is ready, please set device to usb download mode.");
    while (!isDeviceFound)
    {
        uint32_t cost = timer_elapsed(SEARCH_DEVICE_TIMER) / 1000 / 1000;
        if (cost >= SEARCH_DEVICE_TIMEOUT)
            break;

        if (check_usb_download_mode(&usb_dl_dev) == USB_DL_MODE_U_BOOT)
        {
            isDeviceFound = true;
        }
        usleep(100 * 1000);
    }

    if (isDeviceFound)
    {
        LOG_INFO_PRINT("Found Hisi USB Download Device.");
        SY_CHECK(!hisi_usb_dl_open(usb_dl_dev.vendor_id, usb_dl_dev.product_id, device), return FAILURE);
    }
    else
    {
        LOG_WRN_PRINT("Search Hisi USB Download Device timeout!!! exit");
        return FAILURE;
    }

    return SUCCESS;
}

/**
 * @brief USB 反初始化
 *
 * @param device
 * @return int
 */
int usb_dl_deinit(libusb_device_handle *device)
{
    SY_CHECK(!hisi_usb_dl_deinit(device), return FAILURE);

    return SUCCESS;
}

/**
 * @brief 取消选择所有分区
 *
 * @param partitionInfo
 * @return int
 */
int unselect_all_partitions(partition_info_t *partitionInfo)
{
    SY_CHECK(partitionInfo, return FAILURE);
    SY_CHECK(partitionInfo->partitionCount, return FAILURE);

    for (int i = 0; i < partitionInfo->partitionCount; i++)
    {
        partitionInfo->partitions[i].sel = FALSE;
    }

    return SUCCESS;
}

/**
 * @brief 解析传入的自定义分区字符串，选择指定的分区
 *
 * @param custom_partitons
 * @param partitionInfo
 * @return int
 */
int select_custom_partitions(char *custom_partitons, partition_info_t *partitionInfo)
{
    char partition_name[PARTITION_NAME_MAX_LEN] = {0};
    char *str_s = NULL, *str_end = NULL;
    int count = 0;

    SY_CHECK(custom_partitons, return FALSE);
    SY_CHECK(partitionInfo, return FALSE);
    SY_CHECK(partitionInfo->partitionCount, return FALSE);

    // 找到字符串结尾指针
    str_end = strchr(custom_partitons, '\0');
    SY_CHECK(str_end, return FAILURE);

    str_s = custom_partitons;
    while (str_s < str_end)
    {
        char *tmp = NULL;
        tmp = strchr(str_s, custom_partitons_seperator);
        // 找不到分隔符 则跳到字符串结尾
        tmp = (tmp == NULL) ? str_end : tmp;
        if (tmp - str_s > 0)
        {
            memset(partition_name, 0, sizeof(partition_name));
            memcpy(partition_name, str_s, tmp - str_s);
            // 选择指定的分区
            if (!select_partition_by_name(partition_name, TRUE, partitionInfo))
            {
                LOG_DEBUG_PRINT("custom partition %s is foud in burn table.", partition_name);
                count++;
            }
            else
            {
                LOG_ERR_PRINT("custom partition %s is NOT found in burn table!", partition_name);
                return FAILURE;
            }
        }

        // 重新确认字符串起始
        str_s = tmp + 1;
    }

    if (count == 0)
    {
        LOG_ERR_PRINT("Can't find any custom partition in burn table");
        return FAILURE;
    }

    return SUCCESS;
}

/**
 * @brief 检查当前Uboot要烧录的环境是否和固件匹配
 *
 * @param device
 * @param partitionAttr
 * @return int
 */
int usb_dl_check_flash_info(libusb_device_handle *device, usb_dl_common_partation_attr_t *partitionAttr)
{
    int req_len;

    SY_CHECK(device, FAILURE);
    SY_CHECK(partitionAttr, FAILURE);

    /* Flash 类型检查 */
    SY_CHECK(!hisi_usb_dl_device_getinfo(device, reverse_attr_to_string(ATTR_FLASH_TYPE, partitionAttr->flashType)), return FAILURE);

    // TODO Flash 大小检查

    return SUCCESS;
}

/**
 * @brief 检查当前的烧录分区信息表
 *
 * @param partitionInfo
 * @return int
 */
int usb_dl_check_burn_table(partition_info_t *partitionInfo)
{
    partition_t *pstPartition = NULL;

    SY_CHECK(partitionInfo, return FAILURE);

    for (int i = 0; i < partitionInfo->partitionCount; i++)
    {
        pstPartition = &partitionInfo->partitions[i];

        /* 忽略烧录表中未选择的分区 */
        if (pstPartition->sel == TRUE)
        {

            /* 强制忽略 non_upgraded_partitions 中声明的分区 */
            if (is_non_upgraded_partiton(pstPartition->partitionName))
            {
                LOG_WRN_PRINT("partition %-8s is in non_upgraded_partiton!", pstPartition->partitionName);
                SY_CHECK(!select_partition_by_name(pstPartition->partitionName, FALSE, partitionInfo), return FAILURE);
                goto print_unselect;
            }

            LOG_INFO_PRINT("partition %-8s is selected.", pstPartition->partitionName);
        }
        else
        {
        print_unselect:
            LOG_DEBUG_PRINT("partition %-8s is not selected, just skip it!", pstPartition->partitionName);
        }
    }

    return SUCCESS;
}

/**
 * @brief USB 处理
 *
 * @param device
 * @param partitionInfo
 * @return int
 */
int usb_dl_process(libusb_device_handle *device, partition_info_t *partitionInfo)
{
    int partitionCount = partitionInfo->partitionCount;
    partition_t *pstPartition = NULL;
    usb_dl_common_partation_op_attr_t partitionOpAttr = {0};
    usb_dl_common_partation_attr_t *partitionAttr = &partitionOpAttr.partitionAttr;
    char partitionFilePath[255];
    int partitionSize = 0;
    int readSize;

    SY_CHECK(device, return FAILURE);
    SY_CHECK(partitionInfo, return FAILURE);

    // 获取最后一个分区的信息，用于检查当前环境
    pstPartition = &partitionInfo->partitions[partitionCount - 1];
    partitionAttr->flashType = pstPartition->flashType;
    partitionAttr->partitionAddrOffset = pstPartition->startAddr;
    partitionAttr->partitionEraseSize = pstPartition->length;
    SY_CHECK(!usb_dl_check_flash_info(device, partitionAttr), return FAILURE);
    LOG_DEBUG_PRINT("good! check flash info PASS.");
    SY_CHECK(!usb_dl_check_burn_table(partitionInfo), return FAILURE);
    LOG_DEBUG_PRINT("good! check burn table PASS.");

    timer_start(UFU_TIMER);
    for (int i = 0; i < partitionCount; i++)
    {
        pstPartition = &partitionInfo->partitions[i];

        if (pstPartition->sel != TRUE)
            continue;

        partitionAttr->flashType = pstPartition->flashType;
        partitionOpAttr.memoryStartAddr = MEMORY_TEMP_ADDR;
        partitionAttr->partitionAddrOffset = pstPartition->startAddr;
        partitionAttr->partitionEraseSize = pstPartition->length;

        sprintf(partitionFilePath, "%s/%s", partitionInfo->fwDirPath, pstPartition->selectFile);
        if (!is_file_exist(partitionFilePath))
        {
            LOG_ERR_PRINT("partition %s file is not existed, SKIP!", pstPartition->selectFile);
            continue;
        }

        partitionSize = get_binary_file_size(partitionFilePath);
        SY_CHECK(partitionSize > 0, return FAILURE);
        partitionAttr->partitionSize = partitionSize;
        partitionAttr->partitionAligmentSize = ALIGN_UP(partitionSize, PARTITION_ALIGMENT_SIZE);

        partitionOpAttr.partitionContent = malloc(partitionSize);
        readSize = read_binary_file(partitionFilePath, partitionOpAttr.partitionContent, partitionSize);
        SY_CHECK(readSize == partitionSize, goto fail);

        printf("--------------------------------------------\n");
        LOG_INFO_PRINT("upgrade: partition \"%s\" .....", pstPartition->partitionName);
        switch (partitionAttr->flashType)
        {
        case FLASH_TYPE_EMMC:
            LOG_DEBUG_PRINT("it is emmc partition");
            SY_CHECK(!hisi_usb_dl_upgrade_emmc_partation(device, &partitionOpAttr), goto fail);
            break;
        case FLASH_TYPE_SPI:
            LOG_DEBUG_PRINT("it is spi nor partition");
            SY_CHECK(!hisi_usb_dl_upgrade_nor_partation(device, &partitionOpAttr), goto fail);
            break;
        }
        LOG_INFO_PRINT("upgrade: partition \"%s\" success!", pstPartition->partitionName);
        printf("--------------------------------------------\n");

        FREE(partitionOpAttr.partitionContent);
    }

    LOG_INFO_PRINT("awesome! usb dl process success. cost %d ms.", timer_elapsed(UFU_TIMER) / 1000);

    hisi_usb_dl_device_reset(device);
    return SUCCESS;

fail:
    FREE(partitionOpAttr.partitionContent);

    return FAILURE;
}
