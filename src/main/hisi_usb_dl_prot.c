/**
 * @file hisi_usb_dl.c
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief Hisilicon USB Download Protocol
 * @version 0.1
 * @date 2023-11-06
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "common.h"
#include "libusb.h"
#include "hisi_usb_dl_prot.h"
#include "log.h"
#include "timer.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define USB_TRANSFER_TIMEOUT (5000)

#define CMD_BUFFER_LEN (128)
#define REQ_BUFFER_LEN (1024)

#define REQ_EOT_ERROR_STR "[EOT](ERROR)"
#define REQ_EOT_OK_STR "[EOT](OK)"

#define HISI_USB_TIMER 0

#define EMMC_DEF_IDX 0
#define EMMC_DEF_BLOCK_SIZE 512
#define HISI_EMMC_WRITE_BLOCK_CNT (64 * 1024)

static const struct
{
	uint32_t vendor_id;
	uint32_t product_id;
} g_hisi_usb_id[] = {
	{0x1d6b, 0xd001}, // HI3519DV500
	{0x3361, 0xd001}, // HI3519DV500 Demo Board
	{0},
};

typedef enum
{
	CMD_REQ_RET_UNKNOWN,
	CMD_REQ_RET_OK,
	CMD_REQ_RET_ERROR,
} cmd_req_ret_status_e;

static void replace_char(char *str, char target, char replacement)
{
	char *ptr = strchr(str, target);

	while (ptr != NULL)
	{
		*ptr = replacement;
		ptr = strchr(ptr + 1, target);
	}
}

/**
 * @brief 海思USB烧录协议 - 写数据
 *
 * @param device
 * @param data
 * @param data_len
 * @return int
 */
static int hisi_usb_dl_write(libusb_device_handle *device,
							 unsigned char *data, int data_len)
{
	int status, transferred;

	SY_CHECK(device, return FAILURE);
	SY_CHECK(data, return FAILURE);
	SY_CHECK(data_len >= 0, return FAILURE);

	status = libusb_bulk_transfer(device, HISI_USB_DL_BULK_OUT_EP,
								  data, data_len, &transferred, USB_TRANSFER_TIMEOUT);
	if (data_len != transferred)
	{
		LOG_ERR_PRINT("data_len %d is not equal to transferred %d, status %d!", data_len, transferred, status);
		return FAILURE;
	}

	if (status < 0)
	{
		errno = EIO;
		return status;
	}

	return SUCCESS;
}

/**
 * @brief 海思USB烧录协议 - 读数据
 *
 * @param device
 * @param data
 * @param data_len
 * @return int
 */
static int hisi_usb_dl_read(libusb_device_handle *device,
							unsigned char *data, int *data_len)
{
	int status;

	SY_CHECK(device, return FAILURE);
	SY_CHECK(data, return FAILURE);
	SY_CHECK(data_len, return FAILURE);

	status = libusb_bulk_transfer(device, HISI_USB_DL_BULK_IN_EP,
								  data, BUFFER_SIZE, data_len, USB_TRANSFER_TIMEOUT);
	// FIXME transferred len check
	/*
	if (data_len != transferred) {
		printf("data_len %d is not equal to transferred %d, status %d!", data_len, transferred, status);
		return EXIT_FAILURE;
	}*/

	if (status < 0)
	{
		errno = EIO;
		return status;
	}

	return SUCCESS;
}

/**
 * @brief 海思USB烧录协议 处理请求响应
 *
 * @param device
 * @param req
 * @param req_len
 * @return int
 */
static int process_req(libusb_device_handle *device,
					   unsigned char *req, int *req_len)
{
	int ret = 0;
	usb_dl_req_t *pstReq = NULL;

retry:
	if ((ret = hisi_usb_dl_read(device, req, req_len)) != 0)
	{
#ifdef DEBUG
		printf("failt to get data for process req ret = %s", libusb_error_name(ret));
#endif
		if (ret == LIBUSB_ERROR_TIMEOUT)
			goto retry;
		else if (ret == LIBUSB_ERROR_IO)
			goto retry;
		goto exit;
	}

	pstReq = (usb_dl_req_t *)req;

	LOG_DEBUG_PRINT("magic %x, req %s, len %4u (0x%04x)", pstReq->magic, req, *req_len, *req_len);

	/* 替换回车为换行，避免响应日志被覆盖 */
	replace_char(pstReq->data, '\r', '\n');

	switch (pstReq->magic)
	{
	case UCREQ:
		LOG_DEBUG_PRINT("req: CMD REQ.\n***\n%s***", (char *)pstReq->data);
		ret = UCREQ;
		break;
	case UCLREQ:
		LOG_DEBUG_PRINT("req: CMD LOG REQ.\n***\n%s***", (char *)pstReq->data);
		ret = UCLREQ;
		break;
	case UACK:
		LOG_DEBUG_PRINT("req: ACK.");
		ret = UACK;
		break;
	case UQUIRK:
		LOG_DEBUG_PRINT("req: QUIRK.\nlog:\n***\n%s***", (char *)pstReq->data);
		ret = UQUIRK;
		break;
	case 's':
		// start download process.
		if (*req_len == 24)
			LOG_DEBUG_PRINT("req: HELLO.\n***\n%s***", (char *)pstReq);
		goto retry; // 接收到 UHELLO重新接收
		break;
	}

exit:
	return ret;
}

/**
 * @brief 解释 CMD REQ 和 CMD LOG REQ 中的命令执行结果
 *
 * @param req
 * @param req_len
 * @return int <0 : 参数错误 0: 解析失败 1：命令响应为执行成功 2： 命令响应为执行失败
 */
static int parse_cmd_req(unsigned char *req, int req_len)
{
	char *ptr = NULL;

	SY_CHECK(req, return FAILURE);
	SY_CHECK(req_len > 0, return FAILURE);

	ptr = strstr(req, REQ_EOT_ERROR_STR);
	if (ptr == NULL)
	{
		ptr = strstr(req, REQ_EOT_OK_STR);
		if (ptr == NULL)
			return CMD_REQ_RET_UNKNOWN;
		return CMD_REQ_RET_OK;
	}
	return CMD_REQ_RET_ERROR;
}

/**
 * @brief 判断是否是有效的海思SB PID/VID
 *
 * @param vendor_id
 * @param product_id
 * @return int
 */
int hisi_usb_dl_is_valid_usb_id(uint16_t vendor_id, uint16_t product_id)
{
	for (int i = 0; i < ARRARY_SIZE(g_hisi_usb_id); i++)
	{
		if (vendor_id == g_hisi_usb_id[i].vendor_id &&
			product_id == g_hisi_usb_id[i].product_id)
			return TRUE;
	}
	return FALSE;
}

/**
 * @brief 海思USB烧录协议 - 执行Uboot命令
 *
 * @param device
 * @param cmd
 * @return int
 */
int hisi_usb_dl_run_cmd(libusb_device_handle *device, const char *cmd, int req_check)
{

	unsigned char req[REQ_BUFFER_LEN];
	int req_len, status, transferred;
	usb_dl_cmd_t usb_dl_cmd;
	uint8_t len = strlen(cmd);

	SY_CHECK(device, return FAILURE);
	SY_CHECK(cmd, return FAILURE);

	usb_dl_cmd.head = (uint16_t)UCMD;
	usb_dl_cmd.len = len;
	strncpy(usb_dl_cmd.cmd, cmd, sizeof(usb_dl_cmd.cmd));

	/* usb_dl_cmd: head(2 bytes) +len(1 byte) */
	len += 3;

	LOG_DEBUG_PRINT("# exec cmd: %s", usb_dl_cmd.cmd);

#ifdef DEBUG
	printf("* cmd %s , len %4u (0x%04x)", usb_dl_cmd.cmd, (unsigned)len, (unsigned int)len);
#endif

	status = hisi_usb_dl_write(device, (unsigned char *)&usb_dl_cmd, len);
	if (status)
	{
		LOG_ERR_PRINT("ERROR: %s write failed = %d", __func__, status);
		return FAILURE;
	}

	if (req_check == TRUE)
	{
		status = process_req(device, req, &req_len);
		if (status < 0)
		{
			LOG_ERR_PRINT("process_req failed with %d", status);
			return FAILURE;
		}

		SY_CHECK(status == UCREQ ||
					 status == UCLREQ ||
					 status == UQUIRK,
				 return FAILURE);

		SY_CHECK(parse_cmd_req(req, req_len) == CMD_REQ_RET_OK, return FAILURE);
	}

	return SUCCESS;
}

/**
 * @brief 海思USB烧录协议 发送数据(分区/镜像内容)
 *
 * @param device
 * @param data 			： 数据内容
 * @param data_len 		： 数据长度
 * @param append_prefix : 下发boot镜像时使用
 * @return int
 */
int hisi_usb_dl_send_data(libusb_device_handle *device,
						  unsigned char *data, int data_len, int append_prefix)
{
	int status = 0, transferred = 0, current_len, req_len;
	unsigned char req[REQ_BUFFER_LEN];
	usb_dl_data_t usb_dl_data;
	float progress = 0;
	int progressX10 = 0;

	SY_CHECK(device, return FAILURE);
	SY_CHECK(data, return FAILURE);
	// SY_CHECK(data_len <= BUFFER_SIZE, return FAILURE);

	LOG_DEBUG_PRINT("# send data: length %d(0x%x) byte(s)", data_len, data_len);

	while (transferred < data_len)
	{
		if (append_prefix)
		{
			current_len = (transferred + (BUFFER_SIZE - 1) > data_len) ? data_len - transferred : BUFFER_SIZE - 1;
			usb_dl_data.head = UDATA;
			memcpy(usb_dl_data.data, data + transferred, current_len);
			current_len++;
		}
		else
		{
			current_len = (transferred + BUFFER_SIZE) > data_len ? data_len - transferred : BUFFER_SIZE;
			memcpy(&usb_dl_data, data + transferred, current_len);
		}

		status = hisi_usb_dl_write(device, (unsigned char *)&usb_dl_data, current_len);

		if (append_prefix)
			transferred += (current_len - 1);
		else
			transferred += current_len;

		progress = 1.0 * transferred / data_len * 100;
		if (progress >= progressX10)
		{
			progressX10 += 10;
			LOG_DEBUG_PRINT("hisi_usb_dl_send_data: %.2f%%", progress);
		}
#if DEBUG
		printf("[debug]: data_len = %d, transferred=  %d, current_len = %d\n", data_len, transferred, current_len);
#endif
	}

	// 结尾发送 UTAIL
	usb_dl_data.head = UTAIL;
	status = hisi_usb_dl_write(device, (unsigned char *)&usb_dl_data, 1);
	if (status != 0)
	{
		LOG_ERR_PRINT("ERROR: %s write UTAIL failed: %d", __func__, status);
		return FAILURE;
	}

	status = process_req(device, req, &req_len);
	SY_CHECK(status == UACK, return FAILURE);

	return SUCCESS;
}

/**
 * @brief 海思USB烧录协议 切换到命令处理模式
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_switch_to_cmd_mode(libusb_device_handle *device)
{
	unsigned char req[REQ_BUFFER_LEN];
	int req_len, status;
	usb_dl_mode_switch_t usb_dl_mode = {0};

	SY_CHECK(device, return FAILURE);

	LOG_DEBUG_PRINT("# mode switch: CMD");

	usb_dl_mode.head = UHEAD;
	usb_dl_mode.addr = htonl(CMD_MODE_ADDR_SIZE_MAGIC_NUMBER);
	usb_dl_mode.len = htonl(CMD_MODE_ADDR_SIZE_MAGIC_NUMBER);

	status = hisi_usb_dl_write(device, (unsigned char *)&usb_dl_mode, sizeof(usb_dl_mode_switch_t));

	if (status != 0)
	{
		LOG_ERR_PRINT("ERROR: %s write failed: %d", __func__, status);
		return FAILURE;
	}

	status = process_req(device, req, &req_len);
	SY_CHECK(status == UACK, return FAILURE);

	return SUCCESS;
}

/**
 * @brief 海思USB烧录协议 切换到数据传输模式
 *
 * @param device
 * @param addr
 * @param size
 * @return int
 */
int hisi_usb_dl_switch_to_data_mode(libusb_device_handle *device, uint32_t addr, uint32_t len)
{
	unsigned char req[REQ_BUFFER_LEN];
	int req_len, status;
	usb_dl_mode_switch_t usb_dl_mode = {0};

	SY_CHECK(device, return FAILURE);

	LOG_DEBUG_PRINT("# mode switch: DATA");

	usb_dl_mode.head = UHEAD;
	usb_dl_mode.addr = htonl(addr);
	usb_dl_mode.len = htonl(len);

	status = hisi_usb_dl_write(device, (unsigned char *)&usb_dl_mode, sizeof(usb_dl_mode_switch_t));
	if (status != 0)
	{
		LOG_ERR_PRINT("ERROR: %s write failed: %d", __func__, status);
		return FAILURE;
	}

	status = process_req(device, req, &req_len);
	SY_CHECK(status == UACK, return FAILURE);

	return SUCCESS;
}

/**
 * @brief 设备复位
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_device_reset(libusb_device_handle *device)
{
	LOG_INFO_PRINT("the end: reset device");
	SY_CHECK(!hisi_usb_dl_run_cmd(device, "reset", FALSE), return FAILURE);
}

/**
 * @brief 获取Flash信息
 *
 * @param device
 * @param flashTypeStr
 * @return int
 */
int hisi_usb_dl_device_getinfo(libusb_device_handle *device, const char *flashTypeStr)
{
	unsigned char cmd[CMD_BUFFER_LEN];
	unsigned char req[REQ_BUFFER_LEN];
	int req_len;

	SY_CHECK(device, FAILURE);
	SY_CHECK(flashTypeStr, FAILURE);

	// TODO getinfo bootmode 获取当前的启动状态
	sprintf(cmd, "getinfo %s", flashTypeStr);
	SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);

	// TODO 获取flash大小，块大小，类型,ID
	return SUCCESS;
}

/**
 * @brief 更新单个eMMC分区
 *
 * 流程：参考Readme常规分区更新
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_emmc_partation(libusb_device_handle *device,
									   usb_dl_common_partation_op_attr_t *opAttr)
{
	unsigned char cmd[CMD_BUFFER_LEN];
	unsigned char req[REQ_BUFFER_LEN];
	int req_len;
	int ret = 0;
	usb_dl_common_partation_attr_t *partitionAttr = &opAttr->partitionAttr;

	SY_CHECK(device, return FAILURE);
	SY_CHECK(opAttr->partitionContent, return FAILURE);

#define HI_MMC_PARTITION_WRITE_ONCE_SIZE (32 * 1024 * 1024)
	uint32_t writen_size = 0;
	while (writen_size < partitionAttr->partitionAligmentSize)
	{
		/* 0. 切换到命令处理模式 */
		// SY_CHECK(!hisi_usb_dl_switch_to_cmd_mode(device), return FAILURE);

		/* 1. mw.b 0x41000000 0xFF 0x 10000 */
		sprintf(cmd, "mw.b 0x%x 0xFF 0x%x",
				opAttr->memoryStartAddr,
				MIN(HI_MMC_PARTITION_WRITE_ONCE_SIZE, partitionAttr->partitionSize - writen_size));
		SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);

		/* 2. usb download */
		printf("upload data... progress %3.0f%%\n", 100.0 * writen_size / partitionAttr->partitionAligmentSize);
		timer_start(HISI_USB_TIMER);
		SY_CHECK(!hisi_usb_dl_switch_to_cmd_mode(device), return FAILURE);
		SY_CHECK(!hisi_usb_dl_switch_to_data_mode(device,
												  opAttr->memoryStartAddr,
												  MIN(HI_MMC_PARTITION_WRITE_ONCE_SIZE, partitionAttr->partitionSize - writen_size)),
				 return FAILURE);
		SY_CHECK(!hisi_usb_dl_send_data(device,
										(unsigned char *)opAttr->partitionContent + writen_size, MIN(HI_MMC_PARTITION_WRITE_ONCE_SIZE, partitionAttr->partitionSize - writen_size), FALSE),
				 return FAILURE);
		printf(".............. OK! cost %d ms.\n", timer_elapsed(HISI_USB_TIMER) / 1000);

		/* 3. mmc write 0x0 0x41000000 0xa0000 0x10000 */
		printf("write data to partition... \n");
		timer_start(HISI_USB_TIMER);

		sprintf(cmd, "mmc write 0x%x 0x%x 0x%x 0x%x",
				EMMC_DEF_IDX,
				opAttr->memoryStartAddr,
				(partitionAttr->partitionAddrOffset + writen_size) / EMMC_DEF_BLOCK_SIZE,
				MIN((partitionAttr->partitionAligmentSize - writen_size) / EMMC_DEF_BLOCK_SIZE, HISI_EMMC_WRITE_BLOCK_CNT));
		SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);
		writen_size += MIN(partitionAttr->partitionAligmentSize - writen_size,
						   EMMC_DEF_BLOCK_SIZE * HISI_EMMC_WRITE_BLOCK_CNT);
		printf(".......................... OK! cost %d ms.\n", timer_elapsed(HISI_USB_TIMER) / 1000);
	}

	return SUCCESS;
}

/**
 * @brief 更新单个Spi Nor分区
 *
 * 流程：参考Readme常规分区更新
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_nor_partation(libusb_device_handle *device,
									  usb_dl_common_partation_op_attr_t *opAttr)
{
	unsigned char cmd[CMD_BUFFER_LEN];
	unsigned char req[REQ_BUFFER_LEN];
	int req_len;
	int ret = 0;
	usb_dl_common_partation_attr_t *partitionAttr = &opAttr->partitionAttr;

	SY_CHECK(device, return FAILURE);
	SY_CHECK(opAttr->partitionContent, return FAILURE);

	/* 0. 切换到命令处理模式 */
	// SY_CHECK(!hisi_usb_dl_switch_to_cmd_mode(device), return FAILURE);

	/* 1. mw.b 0x41000000 0xFF 0x 10000 */
	sprintf(cmd, "mw.b 0x%x 0xFF 0x%x",
			opAttr->memoryStartAddr,
			partitionAttr->partitionAligmentSize);
	SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);

	/* 2. usb download */
	printf("upload data... \n");
	timer_start(HISI_USB_TIMER);
	SY_CHECK(!hisi_usb_dl_switch_to_cmd_mode(device), return FAILURE);
	SY_CHECK(!hisi_usb_dl_switch_to_data_mode(device, opAttr->memoryStartAddr, partitionAttr->partitionSize), return FAILURE);
	SY_CHECK(!hisi_usb_dl_send_data(device, (unsigned char *)opAttr->partitionContent, partitionAttr->partitionSize, FALSE), return FAILURE);
	printf(".............. OK! cost %d ms.\n", timer_elapsed(HISI_USB_TIMER) / 1000);

	/* 3. sf probe */
	strcpy(cmd, "sf probe 0");
	SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);

	/* 4. sf erase 0x120000 0xe00000 */
	printf("erase partiton... \n");
	timer_start(HISI_USB_TIMER);
	sprintf(cmd, "sf erase 0x%x 0x%x",
			partitionAttr->partitionAddrOffset,
			partitionAttr->partitionEraseSize);
	SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);
	printf("................. OK! cost %d ms.\n", timer_elapsed(HISI_USB_TIMER) / 1000);

	/* 5. sf write 0x41000000 0x1200000 0x6b0000 */
	printf("write data to partition... \n");
	timer_start(HISI_USB_TIMER);
	sprintf(cmd, "sf write 0x%x 0x%x 0x%x",
			opAttr->memoryStartAddr,
			partitionAttr->partitionAddrOffset,
			partitionAttr->partitionAligmentSize);
	SY_CHECK(!hisi_usb_dl_run_cmd(device, cmd, TRUE), return FAILURE);
	printf(".......................... OK! cost %d ms.\n", timer_elapsed(HISI_USB_TIMER) / 1000);

	return SUCCESS;
}

/**
 * @brief 更新boot分区
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_boot_partition(libusb_device_handle *device,
									   usb_dl_boot_partation_op_attr_t *opAttr)
{

	return SUCCESS;
}

/**
 * @brief Libusb & 海思USB设备初始化
 *
 * @param verbose
 * @return int
 */
int hisi_usb_dl_init(int verbose)
{
	int status = 0;

	status = libusb_init(NULL);
	if (status < 0)
	{
		LOG_ERR_PRINT("libusb_init failed: %s", libusb_error_name(status));
		return FAILURE;
	}

	// verbose = LIBUSB_LOG_LEVEL_DEBUG;
	libusb_set_option(NULL, LIBUSB_OPTION_LOG_LEVEL, verbose);

	LOG_DEBUG_PRINT("hisi usb dl init success.");

	return SUCCESS;
}

/**
 * @brief Libusb & 海思USB设备打开设备
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_open(uint16_t vendor_id, uint16_t prodcut_id, libusb_device_handle **device)
{
	int status = 0;

	SY_CHECK(device, return FAILURE);

	libusb_device_handle *device_handle = NULL;

	device_handle = libusb_open_device_with_vid_pid(NULL, vendor_id, prodcut_id);
	if (device_handle == NULL)
	{
		LOG_DEBUG_PRINT("libusb_open_device_with_vid_pid failed");
		return FAILURE;
	}

	// 海思 USB Download 在Linux 下没有对应的驱动，但是保险起见
	libusb_set_auto_detach_kernel_driver(device_handle, 1);
	// 海思 USB Download 只使用 HISI_USB_DOWNLOAD_IF
	status = libusb_claim_interface(device_handle, HISI_USB_DOWNLOAD_IF);
	if (status != LIBUSB_SUCCESS)
	{
		libusb_close(device_handle);
		LOG_ERR_PRINT("libusb_claim_interface: %s", libusb_error_name(status));
		goto err;
	}

	*device = device_handle;
	return SUCCESS;

err:
	libusb_exit(NULL);
	return FAILURE;
}

/**
 * @brief Libusb & 海思USB设备反初始化
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_deinit(libusb_device_handle *device)
{
	libusb_release_interface(device, HISI_USB_DOWNLOAD_IF);
	libusb_close(device);
	libusb_exit(NULL);
}