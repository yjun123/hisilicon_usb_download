#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

// 读取二进制文件内容
int read_binary_file(const char *filePath, char *buffer, int size)
{
    FILE *file = fopen(filePath, "rb");
    if (!file)
    {
        printf("Failed to open file: %s\n", filePath);
        return -1;
    }

    int bytesRead = fread(buffer, sizeof(char), size, file);

    fclose(file);

    return bytesRead;
}

// 写入二进制文件内容
int write_binary_file(const char *filePath, const char *data, int size)
{
    FILE *file = fopen(filePath, "wb");
    if (!file)
    {
        printf("Failed to open file: %s\n", filePath);
        return -1;
    }

    int bytesWritten = fwrite(data, sizeof(char), size, file);

    fclose(file);

    sync();

    return bytesWritten;
}

// 获取二进制文件大小
int get_binary_file_size(const char *filePath)
{
    int ret;
    struct stat statbuf;

    ret = stat(filePath, &statbuf);
    if (ret)
    {
        printf("Failed to get size: %s\n", filePath);
        return -1;
    }

    return statbuf.st_size;
}

// 文件是否存在 1:存在 0:不存在
int is_file_exist(const char *filePath)
{
    struct stat statbuf;

    if (stat(filePath, &statbuf) == 0)
    {
        return 1;
    }

    return 0;
}

// 根据文件路径获取文件名
const char *get_file_name(const char *filePath)
{
    const char *fileName = strrchr(filePath, '/');

    if (fileName != NULL)
        return fileName + 1;
    else
        return filePath;
}

// 根据文件路径获取所在的目录路径
int get_dir_path(const char *path, char *dir)
{
    char *ptr = NULL;
    ptr = strrchr(path, '/');

    if (!ptr)
    {
        memcpy(dir, path, strlen(path));
    }
    else
    {
        memcpy(dir, path, ptr - path);
    }

    return 0;
}