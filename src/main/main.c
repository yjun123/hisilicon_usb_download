/**
 * @file main.c
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief Hisilicon USB Download Tool For Linux
 * @version 0.1
 * @date 2023-11-06
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <sys/types.h>
#include <getopt.h>
#include <unistd.h>

#include "common.h"
#include "usb_dl.h"
#include "xml_parse.h"
#include "log.h"

#define AUTHOR "zngyanj"
#define EMAIL "zngyanj@sunnyoptical.com"

#define VERSION "0.1.0"
#define HISILICON_USB_DOWNLOAD_VERSION (VERSION " (libusb) " \
                                                "BUILD DATE " __DATE__)

static struct option g_long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"list", no_argument, 0, 'l'},
    {"verbose", no_argument, 0, 'v'},
    {"partition", required_argument, 0, 'p'},
    {"version", no_argument, 0, 'V'},
    {0, 0, 0, 0},
};

typedef enum
{
    ONLY_DUMP_BURN_TABLE = BIT(0),
    VERBOSE_LOG_OUTPUT = BIT(1),
    CUSTOM_PARTITIONS = BIT(2),
    SHOW_VERSION = BIT(3),
} option_flag_e;

struct dl_tool
{
    partition_info_t *partitionInfo; // 烧录表分区信息
    libusb_device_handle *usbDevice; // USB设备句柄
} g_dl_tool;

int global_log_level = LOG_LVL_INFO;

const char g_default_xml[] = "nor_burn_table.xml";

void version(void)
{
    printf("Hisilicon USB Download Tool V%s\n", HISILICON_USB_DOWNLOAD_VERSION);
    printf("Author: %s Email: %s\n", AUTHOR, EMAIL);
}

void usage(void)
{
    printf("Usage: ./hisilicon_usb_download [OPTIONS] [burn_table.xml]\n");
    printf("Options:\n");
    printf("-h, --help                  Print help message\n");
    printf("-l, --list                  List partions in xml\n");
    printf("-v, --verbose               Use log verbose mode\n");
    printf("-p, --partition             Specify the partitions to be programmed, separated by a colon (:)\n");
    printf("-V, --version               Print version info and exit\n");
}

int main(int argc, char *argv[])
{
    int ret = SUCCESS;
    char *xmlPath = NULL;
    char *custom_partitions = NULL;
    int opt, opt_idx, option_flag = 0;
    char cwd[256];

    // parse argument
    while ((opt = getopt_long(argc, argv, "hlvp:V", g_long_options, &opt_idx)) != -1)
    {
        switch (opt)
        {
        case 'h':
            usage();
            return EXIT_SUCCESS;
        case 'l':
            option_flag |= ONLY_DUMP_BURN_TABLE;
            break;
        case 'v':
            option_flag |= VERBOSE_LOG_OUTPUT;
            break;
        case 'p':
            option_flag |= CUSTOM_PARTITIONS;
            custom_partitions = optarg;
            break;
        case 'V':
            option_flag |= SHOW_VERSION;
        default:
            break;
        }
    }

    if (option_flag & SHOW_VERSION)
    {
        version();
        return EXIT_SUCCESS;
    }

    xmlPath = argv[optind];
    // 未指定xml的路径
    if (xmlPath == NULL)
    {
        SY_CHECK(getcwd(cwd, sizeof(cwd)) != NULL, return FAILURE);
        xmlPath = strcat(cwd, "/");
        xmlPath = strcat(cwd, g_default_xml);
    }
    SY_CHECK(xmlPath, goto exit);

    if (option_flag & VERBOSE_LOG_OUTPUT)
        log_set_level(LOG_LVL_DEBUG);
    else
        log_set_level(global_log_level);

    // read burn.xml
    g_dl_tool.partitionInfo = malloc(sizeof(partition_info_t));
    if (g_dl_tool.partitionInfo == NULL)
    {
        ret = FAILURE;
        goto exit;
    }
    memset(g_dl_tool.partitionInfo, 0, sizeof(partition_info_t));
    if ((ret = burn_table_read(xmlPath, g_dl_tool.partitionInfo)) != 0)
    {
        LOG_ERR_PRINT("read partition info failed!");
        goto exit;
    }

    if (option_flag & ONLY_DUMP_BURN_TABLE)
    {
        burn_table_dump(g_dl_tool.partitionInfo);
        goto exit;
    }

    if (option_flag & CUSTOM_PARTITIONS)
    {
        SY_CHECK(!unselect_all_partitions(g_dl_tool.partitionInfo), return FAILURE);
        SY_CHECK(!select_custom_partitions(custom_partitions, g_dl_tool.partitionInfo), return FAILURE);
    }

    // usb init
    SY_CHECK(!(ret = usb_dl_init(&g_dl_tool.usbDevice, global_log_level)), goto exit);

    // usb dl process
    if ((ret = usb_dl_process(g_dl_tool.usbDevice, g_dl_tool.partitionInfo)) != 0)
    {
        LOG_ERR_PRINT("usb_dl_process failed");
        goto exit;
    }

exit:
    // usb deinit
    if (g_dl_tool.usbDevice)
        usb_dl_deinit(g_dl_tool.usbDevice);

    FREE(g_dl_tool.partitionInfo);

    return ret;
}