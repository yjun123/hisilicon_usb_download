#ifndef __FILE_IO_H__
#define __FILE_IO_H__

// 读取二进制文件内容
int read_binary_file(const char *filePath, char *buffer, int size);

// 写入二进制文件内容
int write_binary_file(const char *filePath, const char *data, int size);

// 获取二进制文件大小
int get_binary_file_size(const char *filePath);

// 文件是否存在 1:存在 0:不存在
int is_file_exist(const char *filePath);

// 根据文件路径获取文件名
const char *get_file_name(const char *filePath);

// 根据文件路径获取所在的目录路径
int get_dir_path(const char *path, char *dir);
#endif // BINARY_FILE_IO_H