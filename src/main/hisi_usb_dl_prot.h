/**
 * @file hisi_usb_dl.c
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief Hisilicon USB Download Protocol
 * @version 0.1
 * @date 2023-11-06
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#ifndef __HISI_USB_DL_H__
#define __HISI_USB_DL_H__

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define TX_STATE_BUFFER 500
#define TX_STATE_LIMIT 400
#define BUFFER_SIZE 512

// TODO read interface and EP from USB descriptor
#define HISI_USB_DOWNLOAD_IF 0
#define HISI_USB_DL_BULK_OUT_EP 0x01
#define HISI_USB_DL_BULK_IN_EP 0x81

#define HISI_USB_DOWNLOAD_IMANUFACTURER 0x1

typedef enum
{
    UNONE = 0xFF,
    USTART = 0xFA, // START
    UHEAD = 0xFE,  // HEAD
    UDATA = 0xDA,  //
    UTAIL = 0xED,
    UCMD = 0xAB,
    UREQ = 0xFB,
    UACK = 0xAA,
    UNAK = 0x55,
    UCREQ = 0x20,  // CMD REQ
    UCLREQ = 0x0D, // CMD LOG REQ

    UQUIRK = 0x53, // sf erase "SF: 5242880 bytes..."
    UHELLO = 0x73, // "start to d"
} HISI_USB_DL_HEAD_TYPE;

#define CMD_MODE_ADDR_SIZE_MAGIC_NUMBER (0x654af091)

typedef struct
{
    uint16_t head; // 固定为0x00ab
    uint8_t len;
    char cmd[BUFFER_SIZE - 3];
} usb_dl_cmd_t;

typedef struct
{
    uint8_t head; // 更新 boot_image 时 为 0xda,
    char data[BUFFER_SIZE - 1];
} usb_dl_data_t;

typedef struct __attribute__((packed))
{
    uint8_t head; // 固定为UHEAD
    uint32_t len;
    uint32_t addr;
} usb_dl_mode_switch_t;

typedef struct
{
    uint8_t magic;
    uint8_t data[0];
} usb_dl_req_t;

typedef enum
{
    USB_DL_FLASH_TYPE_SPI,
    USB_DL_FLASH_TYPE_NAND,
    // FLASH_TYPE_EMMC,
    USB_DL_FLASH_TYPE_BUTT,
} usb_dl_flash_type_e;

typedef struct
{
    uint32_t flashType;
    uint32_t partitionAddrOffset; // 分区表内指定的分区起始地址
    uint32_t partitionSize;       // 实际的分区文件的大小
    uint32_t partitionEraseSize;  // 分区表内指定的分区大小
} usb_dl_boot_partation_attr_t;

typedef struct
{
    uint8_t *partitionContent;
    usb_dl_boot_partation_attr_t partitionAttr;
} usb_dl_boot_partation_op_attr_t;

typedef struct
{
    uint32_t flashType;
    uint32_t partitionAddrOffset;   // 分区表内指定的分区起始地址
    uint32_t partitionSize;         // 实际的分区文件的大小
    uint32_t partitionAligmentSize; // 字节对齐后的分区文件大小
    uint32_t partitionEraseSize;    // 分区表内指定的分区大小
} usb_dl_common_partation_attr_t;

typedef struct
{
    uint8_t *partitionContent;
    usb_dl_common_partation_attr_t partitionAttr;
    uint32_t memoryStartAddr; // 临时存在分区内容的内存起始地址
} usb_dl_common_partation_op_attr_t;

/**
 * @brief 海思USB烧录协议 - 执行Uboot命令
 *
 * @param device
 * @param cmd
 * @return int
 */
int hisi_usb_dl_run_cmd(libusb_device_handle *device, const char *cmd, int req_check);

/**
 * @brief 海思USB烧录协议 发送数据(分区/镜像内容)
 *
 * @param device
 * @param data 			： 数据内容
 * @param data_len 		： 数据长度
 * @param append_prefix : 下发boot镜像时使用
 * @return int
 */
int hisi_usb_dl_send_data(libusb_device_handle *device,
                          unsigned char *data, int data_len, int append_prefix);

/**
 * @brief 海思USB烧录协议 切换到命令处理模式
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_switch_to_cmd_mode(libusb_device_handle *device);

/**
 * @brief 海思USB烧录协议 切换到数据传输模式
 *
 * @param device
 * @param addr
 * @param size
 * @return int
 */
int hisi_usb_dl_switch_to_data_mode(libusb_device_handle *device, uint32_t addr, uint32_t len);

/**
 * @brief 更新单个eMMC分区
 *
 * 流程：参考Readme常规分区更新
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_emmc_partation(libusb_device_handle *device,
                                       usb_dl_common_partation_op_attr_t *opAttr);

/**
 * @brief 更新单个Spi Nor分区
 *
 * 流程：参考Readme常规分区更新
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_nor_partation(libusb_device_handle *device,
                                      usb_dl_common_partation_op_attr_t *opAttr);

/**
 * @brief 更新boot分区
 *
 * @param device
 * @param opAttr
 * @return int
 */
int hisi_usb_dl_upgrade_boot_partition(libusb_device_handle *device,
                                       usb_dl_boot_partation_op_attr_t *opAttr);

/**
 * @brief 设备复位
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_device_reset(libusb_device_handle *device);

/**
 * @brief 获取Flash信息
 *
 * @param device
 * @param flashTypeStr
 * @return int
 */
int hisi_usb_dl_device_getinfo(libusb_device_handle *device, const char *flashTypeStr);

/**
 * @brief 判断是否是有效的海思SB PID/VID
 *
 * @param vendor_id
 * @param product_id
 * @return int
 */
int hisi_usb_dl_is_valid_usb_id(uint16_t vendor_id, uint16_t product_id);

/**
 * @brief Libusb & 海思USB设备初始化
 *
 * @param verbose
 * @return int
 */
int hisi_usb_dl_init(int verbose);

/**
 * @brief Libusb & 海思USB设备打开设备
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_open(uint16_t vendor_id, uint16_t prodcut_id, libusb_device_handle **device);

/**
 * @brief Libusb & 海思USB设备反初始化
 *
 * @param device
 * @return int
 */
int hisi_usb_dl_deinit(libusb_device_handle *device);

#endif
