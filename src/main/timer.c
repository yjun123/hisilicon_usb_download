#include <sys/time.h>
#include "timer.h"

typedef struct
{
    struct timeval start;
    struct timeval end;
} TIMER;

static TIMER g_timer[10] = {0};

int timer_start(int index)
{
    gettimeofday(&g_timer[index].start, 0);

    return 0;
}

int timer_elapsed(int index)
{
    int elapsed;

    gettimeofday(&g_timer[index].end, 0);

    elapsed = (g_timer[index].end.tv_sec - g_timer[index].start.tv_sec) * 1000000 + (g_timer[index].end.tv_usec - g_timer[index].start.tv_usec);

    return elapsed;
}
