/**
 * @file usb_dl.h
 * @author Yan Jun (zngyanj@sunnyoptical.com)
 * @brief   USB 烧录
 * @version 0.1
 * @date 2023-12-06
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */
#include <libusb.h>
#include <hisi_usb_dl_prot.h>
#include <xml_parse.h>

typedef struct
{
    uint16_t vendor_id;
    uint16_t product_id;
} usb_dl_dev_t;

/**
 * @brief USB 初始化
 *
 * @param device
 * @param verbose
 * @return int
 */
int usb_dl_init(libusb_device_handle **device, int verbose);

/**
 * @brief USB 反初始化
 *
 * @param device
 * @return int
 */
int usb_dl_deinit(libusb_device_handle *device);

/**
 * @brief 取消选择所有分区
 *
 * @param partitionInfo
 * @return int
 */
int unselect_all_partitions(partition_info_t *partitionInfo);

/**
 * @brief 解析传入的自定义分区字符串，选择指定的分区
 *
 * @param custom_partitons
 * @param partitionInfo
 * @return int
 */
int select_custom_partitions(char *custom_partitons, partition_info_t *partitionInfo);

/**
 * @brief 检查当前Uboot要烧录的环境是否和固件匹配
 *
 * @param device
 * @param partitionAttr
 * @return int
 */
int usb_dl_check_flash_info(libusb_device_handle *device, usb_dl_common_partation_attr_t *partitionAttr);

/**
 * @brief USB 处理
 *
 * @param device
 * @param partitionInfo
 * @return int
 */
int usb_dl_process(libusb_device_handle *device, partition_info_t *partitionInfo);
