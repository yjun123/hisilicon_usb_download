/**
 * @file log.c
 * @author Yan Jun (zngyanj@sunnyoptical.com)
 * @brief 调试控制
 * @version 0.1
 * @date 2023-12-11
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */
#include "common.h"
#include "stdio.h"
#include "log.h"

int g_log_level = LOG_LVL_NONE;

static char g_log_level_string[][16] = {
    [LOG_LVL_NONE] = "none",
    [LOG_LVL_ERR] = "err",
    [LOG_LVL_WRN] = "warn",
    [LOG_LVL_INFO] = "info",
    [LOG_LVL_DEBUG] = "debug",
};

static char *log_level2str(int level)
{
    if (level >= LOG_LVL_NONE && level < LOG_LVL_BUTT)
    {
        return g_log_level_string[level];
    }

    return g_log_level_string[LOG_LVL_NONE];
}

/**
 * @brief 设置日志等级
 *
 * @param level
 * @return int
 */
int log_set_level(int level)
{
    if (level >= LOG_LVL_ERR && level < LOG_LVL_BUTT)
    {
        g_log_level = level;
        printf("set global log level = %s\n", log_level2str(level));
        return SUCCESS;
    }

    return FAILURE;
}