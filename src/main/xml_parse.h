/**
 * @file xml_parse.h
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief XML解析子模块
 * @version 0.1
 * @date 2023-11-08
 *
 * Zhejiang Sunny Optical Intelligent Technology Co., Ltd All Right Reserved.
 *
 */

#ifndef _XML_PARSE_H
#define _XML_PARSE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define PARTITION_MAX_NUM 16
#define PARTITION_NAME_MAX_LEN 32
#define PARTITION_FILE_NAME_MAX_LEN 64
#define PARTITION_DIR_PATH_MAX_LEN 127

typedef enum
{
    FLASH_TYPE_SPI,
    FLASH_TYPE_NAND,
    FLASH_TYPE_EMMC,
    FLASH_TYPE_BUTT,
} flash_type_e;

typedef enum
{
    FILE_SYSTEM_NONE,
    FILE_SYSTEM_JFFS2,
    FILE_SYSTEM_EXT4,
    FILE_SYSTEM_BUTT
} file_system_e;

typedef enum
{
    ATTR_FLASH_TYPE,
    ATTR_FILE_SYSTEM,
    ATTR_BUTT,
} attr_enum_e;

typedef struct
{
    _Bool sel;
    char partitionName[PARTITION_NAME_MAX_LEN];
    flash_type_e flashType;
    file_system_e fileSystem;
    uint32_t startAddr;
    uint32_t length;
    char selectFile[PARTITION_FILE_NAME_MAX_LEN];
} partition_t;

typedef struct
{
    int partitionCount;
    partition_t partitions[PARTITION_MAX_NUM];
    char fwDirPath[PARTITION_DIR_PATH_MAX_LEN];
} partition_info_t;

const char *reverse_attr_to_string(attr_enum_e attr_enum, int enum_value);
int burn_table_read(char *xmlPath, partition_info_t *psPartitionInfo);

/**
 * @brief 打印读取解析后的分区烧录信息
 *
 * @param pstPartitionInfo
 */
void burn_table_dump(partition_info_t *pstPartitionInfo);
#endif