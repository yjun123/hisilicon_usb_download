#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xml.h"

#define XML_BUFFER_SIZE 10 * 1024
uint8_t g_xml_buffer[XML_BUFFER_SIZE];

int main(int argc, char **argv)
{
    FILE *fxml;
    int ret;
    char *pcXmlName = NULL;

    if (argc < 2)
        return 1;
    printf("argc %d\n", argc);
    pcXmlName = argv[1];

    fxml = fopen(pcXmlName, "r");
    if (fxml == NULL)
    {
        printf("fopen %s failed\n", pcXmlName);
    }

    fread(g_xml_buffer, sizeof(g_xml_buffer), 1, fxml);
    printf("fread %s size %d\n", pcXmlName, ret);

    printf("xml %s: %s\n", pcXmlName, g_xml_buffer);

    struct xml_document *document = xml_parse_document(g_xml_buffer, strlen(g_xml_buffer));
    if (!document)
    {
        printf("Could parse document\n");
        return EXIT_FAILURE;
    }

    struct xml_node *root = xml_document_root(document);
    size_t node_number = xml_node_children(root);

    for (int i = 0; i < node_number; i++)
    {
        struct xml_node *root_part = xml_node_child(root, i);
        struct xml_string *part = xml_node_name(root_part);

        uint8_t *part_1 = calloc(xml_string_length(part) + 1, sizeof(uint8_t));
        xml_string_copy(part, part_1, xml_string_length(part));

        printf("part %s\n", part_1);

        size_t attribute_size = xml_node_attributes(root_part);
        for (int j = 0; j < attribute_size; j++)
        {
            struct xml_string *attr_name = xml_node_attribute_name(root_part, j);
            struct xml_string *attr_value = xml_node_attribute_content(root_part, j);

            uint8_t *attr_name_1 = calloc(xml_string_length(attr_name) + 1, sizeof(uint8_t));
            uint8_t *attr_value_1 = calloc(xml_string_length(attr_value) + 1, sizeof(uint8_t));

            xml_string_copy(attr_name, attr_name_1, xml_string_length(attr_name));
            xml_string_copy(attr_value, attr_value_1, xml_string_length(attr_value));

            printf("attr_name %s ->   attr_value %s\n", attr_name_1, attr_value_1);
        }
    }
    return 0;
}